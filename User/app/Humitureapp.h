/*******************************************
定义一个头文件用于在wifi窗体功能中引用到温湿度窗体内的函数功能
*******************************************/

#ifndef __HUMITUREAPP__H__
#define __HUMITUREAPP__H__
	#include "stdint.h"
	
	typedef struct
	{
		uint8_t  humi_int;		
		uint8_t  humi_deci;	 	
		uint8_t  temp_int;	 	
		uint8_t  temp_deci;	 	
		uint8_t  check_sum;	 	
	}DHT11_Data_TypeDef;
	
	typedef enum
	{
		TH_ERR=0,
		TH_DS18B20_OK,
		TH_DHT11_OK
	}THRESULT;
	
	extern void TEMP_HUM_GPIO_Config(void);
	extern uint8_t Read_DHT11(DHT11_Data_TypeDef *DHT11_Data);


#endif
